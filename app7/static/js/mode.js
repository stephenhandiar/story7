$("input").click(() => {
    if ($("body").hasClass("light")) {
        $("body").removeClass("light");
        $("body").addClass("dark");
        $("#interactive").html("kalo gasuka ganti lagi aja. <br>");
        $("#actExplain").css("color","#5b7065");
        $("#expExplain").css("color","#5b7065");
        $("#achExplain").css("color","#5b7065");
        $("#act").css("background","#808080");
        $("#exp").css("background","#808080");
        $("#ach").css("background","#808080");
        $("#act").css("color","#333333");
        $("#ach").css("color","#333333");
        $("#exp").css("color","#333333");
    }
    else {
        $("body").removeClass('dark');
        $("body").addClass('light');
        $("#interactive").html("Yuk coba ganti background<br>");
        $("#actExplain").css("color","#333333");
        $("#expExplain").css("color","#333333");
        $("#achExplain").css("color","#333333");
        $("#act").css("background","#333333");
        $("#ach").css("background","#333333");
        $("#exp").css("background","#333333");
        $("#act").css("color","#ffffff");
        $("#ach").css("color","#ffffff");
        $("#exp").css("color","#ffffff");
        
    }
});