from django.test import TestCase, Client
from django.urls import resolve,reverse
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class UnitTest(TestCase):

     def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

     def test_page_uses_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

     def test_landing_use_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

class StorysevenFunctionalTest(TestCase):
   
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StorysevenFunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(StorysevenFunctionalTest, self).tearDown()
    
    def test_klik(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        selenium_buttton1 = selenium.find_element_by_id('act')
        selenium_buttton2 = selenium.find_element_by_id('exp')
        selenium_buttton3 = selenium.find_element_by_id('ach')
        selenium_checkbox = selenium.find_element_by_id('cb')

        selenium_buttton1.click()
        time.sleep(5)
        selenium_buttton2.click()
        time.sleep(5)
        selenium_buttton3.click()
        time.sleep(5)
        selenium_checkbox.click()
        time.sleep(5)
        